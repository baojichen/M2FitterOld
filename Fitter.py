# -*- coding: utf-8 -*-
"""
Created on Fri May  4 14:38:55 2018

@author: Balmer
"""
from analysis import Analyse
from PIL import Image
import os
from PyQt5.QtWidgets import QGraphicsScene, QApplication
from PyQt5.QtWidgets import QGraphicsView
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QSlider, QWidget, QFileDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.pyplot import figure
from PyQt5 import QtCore, QtGui,QtWidgets
from PyQt5.QtCore import Qt
import PyQt5
import copy
import numpy as np
import time
import threading
import pandas as pd
import cv2
import logging
logger = logging.getLogger('mainlogger')

logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
if not len(logger.handlers):
    logger.addHandler(ch)

#define constants
DEFAULT_DIR = os.getcwd()+os.path.sep

class Fitter:

    def __init__(self, parent=None):

        self.analyser = Analyse(self)
        self.analyser.start()
        app = PyuEyeQtApp()
        self.view = PyuEyeQtView()
        self.analyser.directory = self.view.directory
        self.view.Fitter = self
        self.view.pixelSizeText.text()
        self.shouldAutoCrawl = False
        self.shouldMassAnalyse = False
        self.view.show()
        app.exec_()
        self.analyser.stop()

    def startFit(self):

        #if one of the extra features (Realtime M2 or Analyse multiple) is selected then it does not start a fit for a single folder"
        if self.shouldAutoCrawl:
            self.autoCrawler = threading.Thread(target=self.monitorChangesInDir)
            self.autoCrawler.start()
            return
        if self.shouldMassAnalyse:
            self.massAnalyser = threading.Thread(target=self.massAnalyse)
            self.massAnalyser.start()
            return

        #look for every file that if a tif
        fileList = []
        for file in os.listdir(self.view.directory):
            if file.endswith(".tif"):
                  fileList.append(file)

        #create a list of stage positions from that fileList, only valid positions are considered
        stagePosList = self.createStagePosList(fileList)

        #load the images into the programm
        for index in range(len(stagePosList)):

            stagePos = stagePosList[index]
            filename = str(stagePos)+'mm.tif'

            print('Adding image file at pos ',str(stagePos),'to analysing queue')

            imObj = Image.open(self.view.directory + os.path.sep+filename)
            im = np.asarray(imObj)
            FitPath = self.view.directory+os.path.sep+'fit'+os.path.sep
            if not os.path.exists(FitPath):
                os.makedirs(FitPath)

            #if an background image exists, it is automatically subtracted (but not clipped to 0)
            try:
                bkgObj = Image.open(self.view.directory+os.path.sep+'Bkg-'+filename)
                bkg = np.asarray(bkgObj)
                if bkg.shape != im.shape:
                    raise Exception('Background size and image size are not the same with ','bkg.shape',bkg.shape,' im shape',im.shape,' no background subtraction is applied')
                im = cv2.subtract(im,bkg)
                if not os.path.exists(FitPath+os.path.sep+'BkgCorr'+os.path.sep):
                    os.makedirs(FitPath+os.path.sep+'BkgCorr'+os.path.sep)
                cv2.imwrite(FitPath+os.path.sep+'BkgCorr'+os.path.sep+str(stagePos)+"mm"+".tif", im)
            except Exception as exp:
                logging.error(f"startFit: error reading background {exp}")

            #add the images for analysis
            toAnalyser = copy.deepcopy((im,float(self.view.pixelSizeText.text())*1e-3,stagePos,FitPath,self.FloatOrZero(self.view.rotFixedAngle.text())))
            self.analyser.q.put_nowait(toAnalyser)


    def createStagePosList(self,fileList):
        stagePosList = []
        for fID in range(len(fileList)):
            filename = fileList[fID]
            try:
                stagePosList.append(float(filename.split('mm.tif')[0]))
            except:
                continue
        stagePosList.sort()
        return stagePosList

    def changeAutoCrawl(self,state):
           self.shouldAutoCrawl = state

    def massAnalyse(self):
        allFolders = os.listdir(self.view.automatedDirectory)
        for folder in allFolders:
            direc = self.view.automatedDirectory+os.path.sep+folder
            self.analyser.directory = self.view.automatedDirectory+os.path.sep+folder
            fileList = []
            for file in os.listdir(direc):
                if file.endswith(".tif"):
                      fileList.append(file)

            stagePosList = self.createStagePosList(fileList)

            for index in range(len(stagePosList)):

                stagePos = stagePosList[index]
                filename = str(stagePos)+'mm.tif'
                logger.info('Adding image file at pos ',str(stagePos),'to analysing queue')
                imObj = Image.open(direc + os.path.sep+filename)
                im = np.asarray(imObj)
                FitPath = direc+os.path.sep+'fit'+os.path.sep
                if not os.path.exists(FitPath):
                    os.makedirs(FitPath)

                #if an background image exists, it is automatically subtracted (but not clipped to 0)
                try:
                    bkgObj = Image.open(direc+os.path.sep+'Bkg-'+filename)
                    bkg = np.asarray(bkgObj)
                    if bkg.shape != im.shape:
                        raise Exception('massAnalyse: Background size and image size are not the same with ','bkg.shape',bkg.shape,' im shape',im.shape,' no background subtraction is applied')
                    im = cv2.subtract(im,bkg)
                    if not os.path.exists(FitPath+os.path.sep+'BkgCorr'+os.path.sep):
                        os.makedirs(FitPath+os.path.sep+'BkgCorr'+os.path.sep)
                    cv2.imwrite(FitPath+os.path.sep+'BkgCorr'+os.path.sep+str(stagePos)+"mm"+".tif", im)
                except Exception as exp:
                    #print(exp)
                    logging.error(f"Error loading background: {exp}")
                toAnalyser = copy.deepcopy((im,float(self.view.pixelSizeText.text())*1e-3,stagePos,FitPath,self.FloatOrZero(self.view.rotFixedAngle.text())))
                self.analyser.q.put_nowait(toAnalyser)
            while True:
                time.sleep(2)
                if self.analyser.finished:
                    self.analyser.stopFit()
                    self.analyser.empty_M2_fit()
                    break

    '''
    def monitorChangesInDir(self):
        try:
            beforeDir = dict ([(f, None) for f in os.listdir(self.view.automatedDirectory)])
            while self.shouldAutoCrawl:
              time.sleep(0.25)
              afterDir = dict ([(f, None) for f in os.listdir(self.view.automatedDirectory)])
              addedDir = [f for f in afterDir if not f in beforeDir]
              if len(addedDir) > 0:
                  self.view.directory = self.view.automatedDirectory+os.path.sep+addedDir[0]
                  self.analyser.stopFit()
                  self.analyser.empty_M2_fit()
                  print('Using directory ',self.view.directory,' now')
              beforeDir = afterDir
              if 'data.csv' in os.listdir(self.view.directory):
                  fileList = []
                  for file in os.listdir(self.view.directory):
                      if file.endswith(".tif"):
                          fileList.append(file)
                  stagePosList = self.createStagePosList(fileList)
                  for index in range(len(stagePosList)):
                      stagePos = stagePosList[index]
                      filename = str(stagePos)+'mm.tif'
                      print('Adding image file at pos ',str(stagePos),'to analysing queue')
                      imObj = Image.open(self.view.directory + os.path.sep+filename)
                      im = np.asarray(imObj)
                      FitPath = self.view.directory+os.path.sep+'fit'+os.path.sep
                      if not os.path.exists(FitPath):
                        os.makedirs(FitPath)
                      try:
                          bkgObj = Image.open(self.view.directory+os.path.sep+'Bkg-'+filename)
                          bkg = np.asarray(bkgObj)
                          if bkg.shape != im.shape:
                              print('bkg.shape',bkg.shape,'im shape',im.shape)
                              raise Exception('Background size and image size are not the same with ','bkg.shape',bkg.shape,' im shape',im.shape,' no background subtraction is applied')
                          im = im-bkg
                          if not os.path.exists(FitPath+os.path.sep+'BkgCorr'+os.path.sep):
                              os.makedirs(FitPath+os.path.sep+'BkgCorr'+os.path.sep)
                          cv2.imwrite(FitPath+os.path.sep+'BkgCorr'+os.path.sep+str(stagePos)+"mm"+".tif", im)
                      except Exception as exp:
                          print(exp)
                      toAnalyser = copy.deepcopy((im,float(self.view.pixelSizeText.text())*1e-3,stagePos,FitPath,self.FloatOrZero(self.view.rotFixedAngle.text())))
                      self.analyser.q.put_nowait(toAnalyser)
        except Exception as exp:
            print(exp)
    '''
    def monitorChangesInDir(self):
        before = dict ([(f, None) for f in os.listdir(self.view.directory)])
        beforeDir = dict ([(f, None) for f in os.listdir(self.view.automatedDirectory)])
        while self.shouldAutoCrawl:
          time.sleep(0.25)
          afterDir = dict ([(f, None) for f in os.listdir(self.view.automatedDirectory)])
          addedDir = [f for f in afterDir if not f in beforeDir]
          #Looks for new directories which indicate that a new measurement has started
          if len(addedDir) > 0:
              self.view.directory = self.view.automatedDirectory+os.path.sep+addedDir[0]
              self.analyser.stopFit()
              self.analyser.empty_M2_fit()
          beforeDir = afterDir
          #removed = [f for f in before if not f in after]
          after = dict ([(f, None) for f in os.listdir(self.view.directory)])
          added = [f for f in after if not f in before]

          if added:
              stagePosList = []
              for fID in range(len(added)):
                filename = added[fID]
                #Try to find any images
                try:
                    stagePosList.append(float(filename.split('mm.tif')[0]))
                except Exception as exp:
                    continue

              #sort the stage positions from smallest to highest
              stagePosList.sort()

              for index in range(len(stagePosList)):
                stagePos = stagePosList[index]
                filename = str(stagePos)+'mm.tif'
                time.sleep(1)
                imObj = Image.open(self.view.directory +os.path.sep+ filename)
                im = np.asarray(imObj)
                FitPath = self.view.directory+os.path.sep+'fit'+os.path.sep
                if not os.path.exists(FitPath):
                    os.makedirs(FitPath)
                toAnalyser = copy.deepcopy((im,float(self.view.pixelSizeText.text())*1e-3,stagePos,FitPath,self.FloatOrZero(self.view.rotFixedAngle.text())))
                self.analyser.q.put_nowait(toAnalyser)
          before = after



    def FloatOrZero(self, value):
        try:
            return float(value)
        except:
            return 0.0


class PyuEyeQtView(QWidget):

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self.directory = DEFAULT_DIR
        self.image = None
        self.setWindowTitle('Pythonic M2 fitter')
        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(figure())

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.v_layout = QtWidgets.QVBoxLayout(self)
        self.v_layout.addWidget(self.toolbar)
        self.v_layout.addWidget(self.canvas)

        sizeLayout = QtWidgets.QHBoxLayout()

        self.pixelSizeText = QtWidgets.QLineEdit()
        self.pixelSizeText.setText('2.4')
        self.pixelSizeText.setPlaceholderText('Pixel Size in microns')
        sizeLayout.addWidget(self.pixelSizeText)
        self.pixelSizeText.setValidator(QtGui.QDoubleValidator())
        self.pixelSizeText.setMaxLength(4)

        self.v_layout.addLayout(sizeLayout)

        cutLayout = QtWidgets.QHBoxLayout()
        self.gauss1DFitMajCut = QtWidgets.QCheckBox("1D Gauss fit for ratio maj. cut")
        cutLayout.addWidget(self.gauss1DFitMajCut)
        self.gauss1DFitMajCut.clicked.connect(self.clickedGauss1DFitMajCut)

        self.gauss1DFitMinCut = QtWidgets.QCheckBox("1D Gauss fit for ratio min. cut")
        cutLayout.addWidget(self.gauss1DFitMinCut)
        self.gauss1DFitMinCut.clicked.connect(self.clickedGauss1DFitMinCut)

        self.rotFixed = QtWidgets.QCheckBox("Rotation fixed")
        cutLayout.addWidget(self.rotFixed)
        self.rotFixed.clicked.connect(self.clickedRotFixed)
        self.v_layout.addLayout(cutLayout)

        self.rotFixedAngle = QtWidgets.QLineEdit()
        self.rotFixedAngle.setText('')
        cutLayout.addWidget(self.rotFixedAngle)
        self.rotFixedAngle.setPlaceholderText('with angle')
        self.rotFixedAngle.setValidator(QtGui.QDoubleValidator())
        self.rotFixedAngle.setMaxLength(4)

        extraLayout1 = QtWidgets.QHBoxLayout()
        self.updateM2Cont = QtWidgets.QCheckBox("Update M² continuously")
        extraLayout1.addWidget(self.updateM2Cont)
        self.updateM2Cont.clicked.connect(self.clickedUpdateM2Cont)

        self.fixM2Box = QtWidgets.QCheckBox("Fix M²=1 for fit")
        extraLayout1.addWidget(self.fixM2Box)
        self.fixM2Box.clicked.connect(self.clickedfixM2Box)

        self.useMoment = QtWidgets.QCheckBox("Use 2nd moments instead of gaussian fit")
        extraLayout1.addWidget(self.useMoment)
        self.useMoment.clicked.connect(self.clickedUseMoment)

        self.v_layout.addLayout(extraLayout1)

        extraLayout2 = QtWidgets.QHBoxLayout()
        self.plotRes = QtWidgets.QCheckBox("Plot residuals")
        extraLayout2.addWidget(self.plotRes)
        self.plotRes.clicked.connect(self.clickedPlotRes)

        self.automaticDataCrawlBox = QtWidgets.QCheckBox('Realtime M²')
        extraLayout2.addWidget(self.automaticDataCrawlBox)
        self.automaticDataCrawlBox.clicked.connect(self.clickedAutomaticDataCrawl)

        self.massAnalyseBox = QtWidgets.QCheckBox('Multiple datasets')
        extraLayout2.addWidget(self.massAnalyseBox)
        self.massAnalyseBox.clicked.connect(self.clickedMassAnalysis)

        self.setAutomatedDirButton = QtWidgets.QPushButton('Set dir for automated processing')
        extraLayout2.addWidget(self.setAutomatedDirButton)
        self.setAutomatedDirButton.clicked.connect(self.setAutomatedDir)
        self.v_layout.addLayout(extraLayout2)

        extraLayout3 = QtWidgets.QHBoxLayout()
        self.setImageDirButton = QtWidgets.QPushButton('Set dir for only one fit')
        extraLayout3.addWidget(self.setImageDirButton)
        self.setImageDirButton.clicked.connect(self.setImageDir)

        self.saveDataButton = QtWidgets.QPushButton('Save Data')
        extraLayout3.addWidget(self.saveDataButton)
        self.saveDataButton.clicked.connect(self.saveData)
        self.v_layout.addLayout(extraLayout3)

        deleteLayout = QtWidgets.QHBoxLayout()
        self.deleteFitButton = QtWidgets.QPushButton('Stop and delete fit')
        deleteLayout.addWidget(self.deleteFitButton)
        self.deleteFitButton.clicked.connect(self.deleteFit)

        self.startFitButton = QtWidgets.QPushButton('Start fit/automated processing')
        deleteLayout.addWidget(self.startFitButton)
        self.startFitButton.clicked.connect(self.startFit)
        self.v_layout.addLayout(deleteLayout)

        self.processors = []
        self.resize(1280, 720)

    def clickedfixM2Box(self):
        self.Fitter.analyser.fixM2Parameter = self.fixM2Box.isChecked()
    def clickedAutomaticDataCrawl(self):
        self.Fitter.changeAutoCrawl(self.automaticDataCrawlBox.isChecked())
    def clickedMassAnalysis(self):
        self.Fitter.shouldMassAnalyse = self.massAnalyseBox.isChecked()
    def clickedPlotRes(self):
        self.Fitter.analyser.shouldFitRes = self.plotRes.isChecked()
    def clickedUpdateM2Cont(self):
        self.Fitter.analyser.updateM2cont = self.updateM2Cont.isChecked()
    def clickedUseMoment(self):
        self.Fitter.analyser.calcMoment = self.useMoment.isChecked()
    def clickedRotFixed(self):
        self.Fitter.analyser.rotationFixed = self.rotFixed.isChecked()
    def clickedGauss1DFitMajCut(self):
        self.Fitter.analyser.gaussMajCut = self.gauss1DFitMajCut.isChecked()
    def clickedGauss1DFitMinCut(self):
        self.Fitter.analyser.gaussMinCut = self.gauss1DFitMinCut.isChecked()
    def setImageDir(self):
        dialog = QFileDialog.getExistingDirectory(self, 'Image Folder')
        if dialog != '':
            self.directory = dialog
        else:
            self.directory = DEFAULT_DIR
        print('Set directory ',self.directory)
    def setAutomatedDir(self):
        dialog = QFileDialog.getExistingDirectory(self, 'Folder')
        if dialog != '':
            self.automatedDirectory = dialog
        else:
            self.automatedDirectory = DEFAULT_DIR
        print('´Set automated directory ',self.automatedDirectory)
    def deleteFit(self):
        self.Fitter.analyser.stopFit()
        self.Fitter.analyser.empty_M2_fit()
    def startFit(self):
        self.Fitter.startFit()
    def saveData(self):
        self.Fitter.analyser.saveData()
    def add_processor(self, callback):
        self.processors.append(callback)
class PyuEyeQtApp:
    def __init__(self, args=[]):
        self.qt_app = QApplication(args)

    def exec_(self):
        self.qt_app.exec_()

    def exit_connect(self, method):
        self.qt_app.aboutToQuit.connect(method)
Fit = Fitter()